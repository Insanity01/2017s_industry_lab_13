package ictgradschool.industry.lab13.ex01;

/**
 * Created by vwen239 on 18/12/2017.
 */
public class OneRunnable implements Runnable {

    @Override
    public void run() {
        for (int i = 0; i < 1000000; i++) {
            System.out.println(i);
        }

    }
}
