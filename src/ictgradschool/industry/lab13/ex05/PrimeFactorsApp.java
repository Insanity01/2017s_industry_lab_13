package ictgradschool.industry.lab13.ex05;

import ictgradschool.Keyboard;

import java.util.List;

public class PrimeFactorsApp {

    public static void main(String[] args) {

        //asking user for a number
        System.out.println("Print prime factors for which number?");
        String input = Keyboard.readInput();
        System.out.println("Press Q to quit");

        PrimeFactorsTask task = new PrimeFactorsTask(Long.parseLong(input));
//        TaskState state = task.getState();
//        System.out.println(state.toString());

        Thread myThread = new Thread(task);
        myThread.start();

        //using third thread to interrupt computation thread
        Thread abortThread = new Thread(new Runnable() {
            @Override
            public void run() {
                String abort = Keyboard.readInput();
                if(abort.equals("q") || abort.equals("Q")){
                    task.state = TaskState.ABORTED;
                    myThread.interrupt();
                }
                System.out.println("Interrupting calculation");
                myThread.interrupt();
            }
        });
        //Daemon threads will end if all other threads (aka user threads, non-daemon threads) are terminated
        abortThread.setDaemon(true);
        abortThread.start();

        try {
            myThread.join();
//            state = task.getState();
//            System.out.println(state.toString());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        //execute once thread has stopped executing. Without .join() above, this code will just get executed regardless of other thread
            List<Long> result = task.getPrimeFactors();

            for(Long l : result){
                System.out.println("Factor: " + l);
            }


    }
}
