package ictgradschool.industry.lab13.ex05;

import java.util.ArrayList;
import java.util.List;

public class PrimeFactorsTask implements Runnable{

    List<Long> primeFactors;
    Long n;
    TaskState state;

    public PrimeFactorsTask(long n){
        this.n = n;
        primeFactors = new ArrayList<>();
        state = TaskState.INITIALIZED;
    }

    @Override
    public void run() {
        System.out.println("started calculating");
            for (long factor = 2; factor * factor <= n; factor++) {

                // if factor is a factor of n, repeatedly divide it out
                while (n % factor == 0) {
                    //System.out.print(factor + " ");
                    primeFactors.add(factor);
                    System.out.println("n " + n);
                    n = n / factor;
                }

                if(Thread.interrupted()){
                    //if myThread has been interrupted, abort the for loop
                    System.out.println("Aborting");
                    return;
                }
            }
        if(n != 1) {
            primeFactors.add(n);
        }
            System.out.println("factorising complete");
            state = TaskState.COMPLETED;
    }

    public Long n(){
        return n;
    }

    public List<Long> getPrimeFactors() throws IllegalStateException{
        if(state.equals(TaskState.COMPLETED) || state.equals(TaskState.ABORTED)) {
                return primeFactors;
        }
        else{
            throw new IllegalStateException();
        }
    }

    public TaskState getState() {
        return state;
    }
}
