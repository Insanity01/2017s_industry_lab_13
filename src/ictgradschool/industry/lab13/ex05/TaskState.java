package ictgradschool.industry.lab13.ex05;

public enum TaskState {
    INITIALIZED, COMPLETED, ABORTED;
}
